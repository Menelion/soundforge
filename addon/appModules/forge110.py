# Sound Forge
# An app module for NVDA
# Copyright 2014 Joseph Lee and others, released under GPL.

import appModuleHandler

class AppModule(appModuleHandler.AppModule):
	pass
