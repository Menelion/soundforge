# Sony Sound Forge

Author: Joseph Lee

Welcome to Sony Sound Forge for NVDA website. This readme is used to keep track of things to be done for Sound Forge app module.

## What is Sony Sound Forge

Sony Sound Forge is a professional audio recording software that allows you to record sounds and edit audio files. This is similar to programs such as Audacity and GoldWave, with Sound Forge rated as the most powerful of the three apps.

## Current status with NVDA

As of now, no feedback is given when recording is processing, as well as keyboard focus isn't usable in some dialogs.

## Rationale for the app module

* Provide basic support for Sound Forge, including announcing critical parts of the application.
* Discuss accessibility issues with Sony Creative via the app module.

## Authorship and contribution

I (Joseph) am known for various add-ons, including Resource Monitor, StationPlaylist Studio, GoldWave and so on. However, to give newbies a chance to learn about Python and app modules, priority will be given to people new to NVDA development and add-ons. Code contributios are welcome.

